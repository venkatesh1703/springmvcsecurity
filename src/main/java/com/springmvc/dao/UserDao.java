package com.springmvc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springmvc.entity.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
	public User findByEmail(String email);
}

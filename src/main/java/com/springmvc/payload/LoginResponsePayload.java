package com.springmvc.payload;

import com.springmvc.entity.User;

public class LoginResponsePayload {

	private String status;
	private User result;
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getResult() {
		return result;
	}

	public void setResult(User result) {
		this.result = result;
	}

}

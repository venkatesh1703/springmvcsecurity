package com.springmvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springmvc.dao.UserDao;
import com.springmvc.entity.User;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private UserDao userDao;

	@Override
	public User login(User user) {

		if (user.getEmail().isEmpty() || user.getPassword().isEmpty()) {
			return null;
		} else {
			User userobj = userDao.findByEmail(user.getEmail());
			System.out.println(userobj);
			if(userobj != null && userobj.getPassword().equals(user.getPassword())) {
				return userobj;
			} else {
				return null;
			}
		}
	}

}

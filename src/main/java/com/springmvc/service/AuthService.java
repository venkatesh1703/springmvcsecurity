package com.springmvc.service;

import com.springmvc.entity.User;

public interface AuthService {
	
	public User login(User user);

}

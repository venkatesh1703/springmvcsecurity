package com.springmvc.config;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.springmvc.entity.User;

/**
 * 
 * @author sthummalapally
 *
 */

@Component
public class JwtTokenFilter extends GenericFilterBean {

	private JwtTokenProvider jwtTokenProvider;

	public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {
		String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);

		String username = jwtTokenProvider.validateToken(token);
		if (!StringUtils.isEmpty(username)) {
			User userDetails = new User();
			userDetails.setEmail(username);
			Authentication auth = new UsernamePasswordAuthenticationToken(userDetails,"");
			SecurityContextHolder.getContext().setAuthentication(auth);
		} else {
			HttpServletResponse httpResponse = (HttpServletResponse) res;
			Map<String, Object> response = new LinkedHashMap<>();
			response.put("status_code", 401);
			response.put("status", "UNAUTHORIZED");
			ObjectMapper objMapper = new ObjectMapper();
			PrintWriter out = httpResponse.getWriter();
			httpResponse.setContentType("application/json");
			httpResponse.setCharacterEncoding("UTF-8");
			out.print(objMapper.writeValueAsString(response));
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			return;
		}

		filterChain.doFilter(req, res);
	}

}
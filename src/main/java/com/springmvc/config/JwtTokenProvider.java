package com.springmvc.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;


/**
 * 
 * @author sthummalapally
 *
 */
@Component
public class JwtTokenProvider {

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public String validateToken(String token) {
		return JwtImpl.isValidUser(token);
	}

}
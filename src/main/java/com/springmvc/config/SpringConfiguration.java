package com.springmvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@EnableWebSecurity
@Configuration
@ComponentScan(basePackages = "com.springmvc")
public class SpringConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

	private final long MAX_AGE_SECS = 3600;
	private static final String[] AUTH_WHITELIST = { "/SpringMvcSecurity/auth/**" };

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*")
				.allowedMethods("HEAD", "OPTIONS", "GET", "POST", "PUT", "PATCH", "DELETE").maxAge(MAX_AGE_SECS);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		JwtTokenFilter customFilter = new JwtTokenFilter(jwtTokenProvider());
		http.cors().and()
		.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class)
		.httpBasic().disable()
		.csrf().disable()
		.authorizeRequests().antMatchers("/SpringMvcSecurity/user/**").authenticated();

	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(AUTH_WHITELIST);
	}
	
	@Bean
	public JwtTokenProvider jwtTokenProvider() {

		return new JwtTokenProvider();
	}
}

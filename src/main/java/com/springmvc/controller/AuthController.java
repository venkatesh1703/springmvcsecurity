package com.springmvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springmvc.config.JwtImpl;
import com.springmvc.entity.User;
import com.springmvc.payload.LoginResponsePayload;
import com.springmvc.service.AuthService;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private AuthService service;

	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody User user) {
		LoginResponsePayload response = new LoginResponsePayload();
		User userobj = service.login(user);
		if (userobj != null) {
			String token = JwtImpl.createJWT(userobj.getEmail(), 60 * 60 * 1000);
			response.setStatus("Login success");
			response.setToken(token);
			response.setResult(userobj);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		} else {
			response.setStatus("Invalid credentials");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

	}
}
